# Scripts d'exemple pour utiliser le modèle Autonomix 

Ce dossier fournit les programmes d'exemple permettant d'utiliser le package du modèle de microsimulation Autonomix version 1 de décembre 2021. 

Le fichier `config_template.R` est un script permettant de lancer le modèle à partir des données de l'enquête CARE ou des Remontées Individuelles sur l'APA (disponibles au CASD), mais aussi à partir des Remontées Individuelles sur l'APA floutées, disponibles en open data sur le site de la DREES.  

Le fichier `config_template_base_exemple.R` permet de lancer la simulation sur la base complètement fictive disponible dans le sous-dossier `base_exemple` de ce projet, pour donner un exemple rapide du fonctionnement du code. 

Le fichier `telechargement_base_floutee` permet de télécharger directement les données des Remontées Individuelles sur l'APA floutées, directement depuis le site de la DREES. 

Lien vers la documentation du modèle : XXXXXXXXXXXX

Présentation de la Drees : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères. 
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : Ce package utilise les données de la base CARE Ménage 2015, ou des RI 2017 ou des RI 2017 floutées dans leur version du 23 décembre 2021. Traitements : Drees.

Date de la dernière exécution des programmes avant publication, et version des logiciels utilisés : Les programmes ont été exécutés le 14/12/2021 avec la version 4.0.5 de R. Les packages mobilisés sont les packages ATNMXOutils et ATNMXDomicile, disponibles au téléchargement ici : <lien téléchargement packages>. 
